package ru.pcs.web.onlinestore.forms;

import lombok.Data;

@Data
public class SignUpForm {
    private String lastName;
    private String firstName;
    private Integer age;
    private String login;
    private String password;
}
