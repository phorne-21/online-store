package ru.pcs.web.onlinestore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pcs.web.onlinestore.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}

