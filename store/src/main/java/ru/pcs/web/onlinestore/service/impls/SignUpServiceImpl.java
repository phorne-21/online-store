package ru.pcs.web.onlinestore.service.impls;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pcs.web.onlinestore.entity.User;
import ru.pcs.web.onlinestore.forms.SignUpForm;
import ru.pcs.web.onlinestore.repository.UserRepository;
import ru.pcs.web.onlinestore.service.SignUpService;

@RequiredArgsConstructor
@Service
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public void signUpUser(SignUpForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .age(form.getAge())
                .role(User.Role.USER)
                .login(form.getLogin())
                .password(passwordEncoder.encode(form.getPassword()))
                .build();

        userRepository.save(user);
    }
}
