package ru.pcs.web.onlinestore.service;


import ru.pcs.web.onlinestore.entity.Product;
import ru.pcs.web.onlinestore.forms.ProductForm;

import java.util.List;

public interface ProductService {

    void addProduct(ProductForm form);

    void update(Long id, ProductForm productForm);

    List<Product> getAllProducts();

    void deleteProduct(Long productId);

    Product getProduct(Long productId);
}

