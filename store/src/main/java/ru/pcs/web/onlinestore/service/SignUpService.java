package ru.pcs.web.onlinestore.service;

import ru.pcs.web.onlinestore.forms.SignUpForm;

public interface SignUpService {
    void signUpUser(SignUpForm form);
}
