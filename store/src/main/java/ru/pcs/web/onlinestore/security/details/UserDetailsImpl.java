package ru.pcs.web.onlinestore.security.details;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.pcs.web.onlinestore.entity.User;

import java.util.Collection;
import java.util.Collections;

public class UserDetailsImpl implements UserDetails {

    private final User user;

    public UserDetailsImpl(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = user.getRole().toString();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;//не просрочен ли аккаунт
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;//не заблокирован ли аккаунт
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;//не просрочен ли пароль
    }

    @Override
    public boolean isEnabled() {
        return true;//активный ли пользователь
    }
}
