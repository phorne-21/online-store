package ru.pcs.web.onlinestore.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.web.onlinestore.entity.Product;
import ru.pcs.web.onlinestore.forms.ProductForm;
import ru.pcs.web.onlinestore.service.ProductService;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class ProductController {

    private final ProductService productService;

    @GetMapping("/products")
    public String getProductsPage (Model model){
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Long productId) {
        Product product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "product_admin";
    }

    @PostMapping("/products")
    public String addProduct (ProductForm form) {
        productService.addProduct(form);
        return "redirect:/products";
    }

    @PostMapping("products/{product-id}/delete")
    public String deleteProduct(@PathVariable ("product-id") Long productId){
        productService.deleteProduct(productId);
        return "redirect:/products";
    }

    @PostMapping("products/{product-id}/update")
    public String update(@PathVariable ("product-id") Long productId, ProductForm productForm){
        productService.update(productId, productForm);
        return "redirect:/products/{product-id}";
    }
}
