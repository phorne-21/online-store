package ru.pcs.web.onlinestore.forms;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Data
public class ProductForm {

    @NotEmpty
    private String name;

    @NotNull
    private BigDecimal price;

    @NotNull
    private Integer count;
}

