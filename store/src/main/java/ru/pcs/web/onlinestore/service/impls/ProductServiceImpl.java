package ru.pcs.web.onlinestore.service.impls;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.onlinestore.entity.Product;
import ru.pcs.web.onlinestore.forms.ProductForm;
import ru.pcs.web.onlinestore.repository.ProductRepository;
import ru.pcs.web.onlinestore.service.ProductService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productsRepository;

    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .name(form.getName())
                .price(form.getPrice())
                .count(form.getCount())
                .build();
        productsRepository.save(product);
    }

    @Override
    public void update(Long id, ProductForm productForm) {
        Product product = productsRepository.getById(id);
        product.setName(productForm.getName());
        product.setPrice(productForm.getPrice());
        product.setCount(productForm.getCount());
        productsRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public void deleteProduct(Long productId) {
        productsRepository.deleteById(productId);
    }

    @Override
    public Product getProduct(Long productId) {
        return productsRepository.getById(productId);
    }
}

